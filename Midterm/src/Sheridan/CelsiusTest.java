
/*
 * Author: Jeel Patel
 * SID:991548626 
 */
package Sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testIsValidRegular() {
		int x = Celsius.fromfahrenheit(32);
		assertTrue("", x==0);
	}
@Test
	public void testIsExceptional() {
		int x=Celsius.fromfahrenheit(25);
		assertFalse("",x>0);
	}
	
@Test
	public void testIsBoundaryIn() {
		int x=Celsius.fromfahrenheit(56);
		assertTrue("",x==0);
	}
@Test
	public void testIsBoundaryOut() {
		int x=Celsius.fromfahrenheit(57);
		assertTrue("",x==0);
	}
}
